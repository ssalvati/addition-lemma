Require Export Coq.Bool.Bool.
Require Export Coq.Bool.Bvector.
Export Coq.Bool.Bvector.BvectorNotations.
Require Vector.
Export Vector.VectorNotations.
Require Export Coq.Logic.Decidable.
Require Import Lia.


Set Implicit Arguments.

Check @Vector.rect2.

(** We start by simple inversion lemmas for vector with 0 and at least one
element. *)

Lemma inv_vec0: forall A (v: Vector.t A 0), v = Vector.nil A.
Proof.
  intros A v.
  now apply Vector.case0 with (v:=v).
Qed.

Lemma inv_vecS n: forall A (v: Vector.t A (S n)), v = Vector.cons A (Vector.hd v) _ (Vector.tl v).
Proof.
  intros A v.
  now apply Vector.caseS with (v:=v).
Qed.




(** We now turn to operations on vectors. To simplify the proof, we define
tactics that allows for simultaneous induction on 2 or 3 vectors of the same
dimension. *)

Ltac vin2 v1 v2 :=
  let n := fresh "n" in
  let w1 := fresh "v1'" in
  let w2 := fresh "v2'" in
  let rect := fresh "rect" in
  let x1 := fresh "x1" in
  let x2 := fresh "x2" in
  apply Vector.rect2 with (v1:=v1) (v2:=v2);
  [| intros n w1 w2 rect x1 x2].


Definition rect3 {A B C}:
  forall P : forall n : nat, Vector.t A n -> Vector.t B n -> Vector.t C n -> Type,
    P 0 [] [] [] ->
    (forall (n : nat) (v1 : Vector.t A n) (v2 : Vector.t B n) (v3 : Vector.t C n),
        P n v1 v2 v3-> forall (a : A) (b : B) (c : C), P (S n) (a :: v1) (b :: v2) (c :: v3)) ->
    forall (n : nat) (v1 : Vector.t A n) (v2 : Vector.t B n) (v3: Vector.t C n), P n v1 v2 v3.
  intros.
  generalize dependent v3.
  apply Vector.rect2 with (v1:=v1) (v2:=v2); intros.
  - now rewrite inv_vec0.
  - rewrite inv_vecS.
    now apply X0.
Defined.

Ltac vin3 v1 v2 v3 :=
  let n := fresh "n" in
  let w1 := fresh "v1'" in
  let w2 := fresh "v2'" in
  let w3 := fresh "v3'" in
  let rect := fresh "rect" in
  let x1 := fresh "x1" in
  let x2 := fresh "x2" in
  let x3 := fresh "x3" in
  apply rect3 with (v1:=v1) (v2:=v2) (v3:=v3);
  [| intros n w1 w2 w3 rect x1 x2 x3].


Lemma xor_and_distr:
  forall n (v1 v2 v3:Bvector n),
    (v1 ^⊕ v2) ^& v3 = (^~ v1 ^& v2 ^& v3) ^| (v1 ^& ^~ v2 ^& v3).
Proof.
  intros n v1 v2 v3.
  vin3 v1 v2 v3.
  - reflexivity.
  - simpl.
    rewrite rect.
    now destruct x1, x2, x3.
Qed.


Lemma xor_or_distr:
  forall n (v1 v2 v3:Bvector n),
    v1 ^⊕ (v2 ^| v3) = (^~ v1 ^& (v2 ^| v3)) ^| (v1 ^& ^~ v2 ^& ^~v3).
Proof.
  intros n v1 v2 v3.
  vin3 v1 v2 v3.
  - reflexivity.
  - simpl.
    rewrite rect.
    now destruct x1, x2, x3.
Qed.



Fixpoint FALSE (n:nat): Bvector n:=
  match n with
  | O => []
  | S n' => false :: FALSE n'
  end.

Notation "⊥_ n" := (FALSE n) (at level 1, format "⊥_ n") : vector_scope.

Fixpoint TRUE (n:nat): Bvector n:=
  match n with
  | O => []
  | S n' => true :: TRUE n'
  end.

Notation "⊤_ n" := (TRUE n) (at level 1, format "⊤_ n") : vector_scope.


Lemma neg_true_false:
  forall n, ^~ ⊤_n = ⊥_n.
Proof.
  induction n.
  - trivial.
  - simpl.
    now rewrite IHn.
Qed.

Lemma neg_false_true:
  forall n, ^~ ⊥_n = ⊤_n.
Proof.
  induction n.
  - trivial.
  - simpl.
    now rewrite IHn.
Qed.


Lemma and_false:
  forall n (v:Bvector n),
    v ^& ⊥_n = ⊥_n.
Proof.
  intros n v.
  induction v.
  - reflexivity.
  - simpl.
    rewrite IHv.
    now destruct h.
Qed.

Lemma and_assoc:
  forall n (v1 v2 v3:Bvector n),
    v1 ^& v2 ^& v3 = v1 ^& (v2 ^& v3).
Proof.
  intros n v1 v2 v3.
  vin3 v1 v2 v3.
  - reflexivity.
  - simpl.
    rewrite rect.
    now destruct x1, x2, x3.
Qed.

Lemma and_comm:
  forall n (v1 v2:Bvector n),
    v1 ^& v2 = v2 ^& v1.
Proof.
  intros n v1 v2.
  vin2 v1 v2.
  - reflexivity.
  - simpl.
    rewrite rect.
    now rewrite andb_comm.
Qed.


Lemma or_assoc:
  forall n (v1 v2 v3:Bvector n),
    v1 ^| v2 ^| v3 = v1 ^| (v2 ^| v3).
Proof.
  intros n v1 v2 v3.
  vin3 v1 v2 v3.
  - reflexivity.
  - simpl.
    rewrite rect.
    now destruct x1, x2, x3.
Qed.

Lemma or_comm:
  forall n (v1 v2:Bvector n),
    v1 ^| v2 = v2 ^| v1.
Proof.
  intros n v1 v2.
  vin2 v1 v2.
  - reflexivity.
  - simpl.
    rewrite rect.
    now rewrite orb_comm.
Qed.


Lemma xor_assoc:
  forall n (v1 v2 v3:Bvector n),
    v1 ^⊕ v2 ^⊕ v3 = v1 ^⊕ (v2 ^⊕ v3).
Proof.
  intros n v1 v2 v3.
  vin3 v1 v2 v3.
  - reflexivity.
  - simpl.
    rewrite rect.
    now destruct x1, x2, x3.
Qed.

Lemma xor_comm:
  forall n (v1 v2:Bvector n),
    v1 ^⊕ v2 = v2 ^⊕ v1.
Proof.
  intros n v1 v2.
  vin2 v1 v2.
  - reflexivity.
  - simpl.
    rewrite rect.
    now rewrite xorb_comm.
Qed.

Lemma de_morgan_and:
  forall n (v1 v2: Bvector n),
    ^~(v1 ^& v2) = ^~v1 ^| ^~ v2.
Proof.
  intros n v1 v2.
  vin2 v1 v2.
  - reflexivity.
  - simpl.
    now rewrite negb_andb, rect.
Qed.

Lemma de_morgan_or:
  forall n (v1 v2: Bvector n),
    ^~(v1 ^| v2) = ^~v1 ^& ^~ v2.
Proof.
  intros n v1 v2.
  vin2 v1 v2.
  - reflexivity.
  - simpl.
    now rewrite negb_orb, rect.
Qed.


Lemma false_or:
  forall n (v:Bvector n),
    ⊥_n ^| v = v.
Proof.
  intros n v.
  induction v.
  - reflexivity.
  - simpl.
    now rewrite IHv.
Qed.

Lemma true_and:
  forall n (v:Bvector n),
    ⊤_n ^& v = v.
Proof.
  intros n v.
  induction v.
  - reflexivity.
  - simpl.
    now rewrite IHv.
Qed.

Lemma or_false:
  forall n (v:Bvector n),
    v ^| ⊥_n = v.
Proof.
  intros n v.
  rewrite or_comm.
  now rewrite false_or.
Qed.

Lemma and_true:
  forall n (v:Bvector n),
    v ^& ⊤_n = v.
Proof.
  intros n v.
  rewrite and_comm.
  now rewrite true_and.
Qed.

Lemma double_neg:
  forall n (v:Bvector n),
    ^~ ^~ v = v.
Proof.
  intros n v; induction v.
  - reflexivity.
  - simpl.
    now rewrite negb_involutive, IHv.
Qed.

Lemma negb_eq:
  forall (b1 b2 :bool),
    negb b1 = b2 <-> b1 = negb b2.
Proof.
  intros [] []; simpl; split; intros Eq; try reflexivity; discriminate.
Qed.

Lemma neg_eq:
  forall n (v1 v2:Bvector n),
    ^~ v1 = v2 <-> v1 = ^~ v2.
Proof.
  intros n v1 v2.
  vin2 v1 v2.
  - reflexivity.
  - simpl.
    destruct rect.
    split; intros Eq; destruct (Vector.cons_inj Eq).
    + apply  negb_eq in H1.
      now rewrite H1, (H H2).
    + apply negb_eq in H1.
      now rewrite H1, (H0 H2).
Qed.

Lemma andb_disjoint:
  forall (x1 x2 :bool),
    x1 && x2 = false <-> negb x1 && x2 = x2.
Proof.
  intros [] []; simpl; try reflexivity.
  - split; intros Eq; now rewrite Eq.
  - split; reflexivity.
Qed.

Lemma orb_included:
  forall (x1 x2 :bool),
    x1 || x2 = x2 <-> x1 && negb x2 = false.
Proof.
  intros [] []; simpl; try reflexivity; split; reflexivity.
Qed.


Lemma conj_disjoint:
  forall n (v1 v2 : Bvector n),
    v1 ^& v2 = ⊥_n -> ^~ v1 ^& v2 = v2.
Proof.
  intros n v1 v2.
  vin2 v1 v2; intros Eq; simpl in *.
  - reflexivity.
  - destruct (Vector.cons_inj Eq).
    rewrite (rect H0).
    apply andb_disjoint in H.
    now rewrite H.
Qed.

Lemma inclusion_dual_disjoint:
  forall n (v1 v2 : Bvector n),
    v1 ^| v2 = v2 <-> v1 ^& ^~v2 = ⊥_n.
Proof.
  intros n v1 v2.
  vin2 v1 v2.
  - reflexivity.
  - simpl.
    destruct rect.
    split; intros Eq; destruct (Vector.cons_inj Eq).
    + apply orb_included in H1.
      now rewrite H1, (H H2).
    + apply orb_included in H1.
      now rewrite H1, (H0 H2).
Qed.
      


Lemma xor_and_distr_disjoint:
  forall n (v1 v2 v3:Bvector n),
    v2 ^& v3 = ⊥_n ->
    (v1 ^⊕ v2) ^& v3 = (v1 ^& v3).
Proof.
  intros n v1 v2 v3 Eq.
  rewrite xor_and_distr.
  repeat rewrite and_assoc.
  rewrite Eq, (conj_disjoint _ _ Eq).
  now rewrite and_false, false_or.
Qed.

Lemma xor_disj:
  forall n (v1 v2:Bvector n),
    v1 ^⊕ (v1 ^| v2) = ^~v1 ^& v2.
Proof.
  intros n v1 v2.
  vin2 v1 v2.
  - reflexivity.
  - simpl.
    rewrite rect.
    destruct x1 eqn:eq1, x2 eqn:eq2; trivial.
Qed.

Lemma xor_conj:
  forall n (v1 v2:Bvector n),
    (v1 ^⊕ v2) ^& ^~v2 = v1 ^& ^~v2.
Proof.
  intros n v1 v2.
  vin2 v1 v2.
  - reflexivity.
  - simpl.
    rewrite rect.
    destruct x1 eqn:eq1, x2 eqn:eq2; trivial.
Qed.



Lemma and_or_distr:
  forall n (v1 v2 v3:Bvector n),
    v1 ^& (v2 ^| v3) = v1 ^& v2 ^| v1 ^& v3.
Proof.
  intros n v1 v2 v3.
  vin3 v1 v2 v3.
  - reflexivity.
  - simpl.
    rewrite rect.
    destruct x1 eqn:eq1, x2 eqn:eq2; trivial.
Qed.

Lemma and_idempotent:
  forall n (v:Bvector n),
    v ^& v  = v.
Proof.
  intros n v.
  induction v; trivial.
  simpl.
  rewrite IHv.
  now destruct h.
Qed.

Lemma or_idempotent:
  forall n (v:Bvector n),
    v ^| v  = v.
Proof.
  intros n v.
  induction v; trivial.
  simpl.
  rewrite IHv.
  now destruct h.
Qed.


Lemma or_absorb:
  forall n (v1 v2 : Bvector n),
    v1 ^| (v1 ^| v2) = v1 ^| v2.
Proof.
  intros n v1 v2.
  rewrite <- or_assoc.
  now rewrite or_idempotent.
Qed.

(* Lemma inclusion_disjoint_duality: *)
(*   forall n (P : Bvector n -> Bvector n -> Prop), *)
(*     (forall (v1 v2 : Bvector n), (v1 ^& v2 = ⊥_n -> P v1 v2)) <-> *)
(*       (forall (v1 v2 : Bvector n),(v1 ^| v2 = v2 -> P v1 (^~ v2))). *)
(*   Proof. *)
(*     intros n P. *)
(*     split; intros H v1 v2. *)
(*     - rewrite inclusion_dual_disjoint. *)
(*       now apply H. *)
(*     - intros Eq. *)
(*       apply conj_disjoint in Eq. *)
(*       rewrite <- (double_neg v2) in Eq at 1. *)
(*       rewrite <- de_morgan_or in Eq. *)
(*       rewrite <- Eq. *)
(*       apply H. *)
(*       now rewrite or_absorb. *)
(* Qed. *)



(* Lemma xor_comm: *)
(*   forall n (v1 v2:Bvector n), *)
(*     v1 ^⊕ v2 = v2 ^⊕ v1. *)
(* Proof. *)
(*   intros n v1 v2. *)
(*   vin2 v1 v2; trivial. *)
(*   simpl; rewrite rect. *)
(*   now destruct x1, x2. *)
(* Qed. *)

Lemma xor_false:
  forall n (v: Bvector n),
    v ^⊕ ⊥_n = v.
Proof.
  intros n v.
  induction v; trivial.
  simpl.
  rewrite IHv.
  now destruct h.
Qed.


Lemma xor_nilpotent:
  forall n (v: Bvector n),
    v ^⊕ v = ⊥_n.
Proof.
  intros n v.
  induction v; trivial.
  simpl.
  rewrite IHv.
  now destruct h.
Qed.

Lemma and_andb:
  forall n (v1 v2:Bvector n) (p:Fin.t n),
    (v1 ^& v2)[@ p] = v1[@p] && v2[@p].
Proof.
  intros n v1 v2.
  vin2 v1 v2.
  - intros p .
    inversion p .
  -Search (Fin.t (S _)).
   intros p.
   apply (Fin.caseS' p).
   + reflexivity.
   + now simpl.
Qed.

Lemma and_contra:
  forall n (v:Bvector n),
    v ^& ^~v = ⊥_n.
Proof.
  intros n v.
  induction v; trivial.
  simpl.
  rewrite IHv.
  now destruct h.
Qed.


Lemma neg_negb:
  forall n (v:Bvector n) (p:Fin.t n),
    (^~ v)[@ p] = negb (v[@p]).
Proof.
  intros n v.
  induction v.
  - intros p; inversion p.
  - intros p.
    apply (Fin.caseS' p).
    + reflexivity.
    + now simpl.
Qed.

Lemma neg_involutive:
  forall n (v:Bvector n),
    ^~(^~v) = v.
Proof.
  intros n v.
  induction v.
  - reflexivity.
  - simpl.
    rewrite IHv.
    now destruct h.
Qed.


Lemma not_and_disjoint:
  forall n (v1 v2:Bvector n),
    v1 ^& v2 = ⊥_n ->
    ^~ v1 ^& v2 = v2.
Proof.
  intros n v1 v2.
  vin2 v1 v2.
  - reflexivity.
  - simpl.
    intro Eq.
    destruct (Vector.cons_inj Eq).
    rewrite (rect H0).
    destruct x1 eqn:eq1, x2 eqn:eq2; trivial.
    discriminate.
Qed.

Lemma not_and_included:
  forall n (v1 v2:Bvector n),
    v1 ^| v2 = v2 ->
    ^~ v1 ^& ^~ v2 =  ^~ v2.
Proof.
  intros n v1 v2.
  rewrite inclusion_dual_disjoint.
  apply not_and_disjoint.
Qed.

Lemma disjoint_and_or_r: forall n (v1 v2 : Bvector n),
    v1 ^& v2 = ⊥_n -> v1 ^& (v1 ^| v2) = v1.
Proof.
  intros n v1 v2 eq.
  rewrite and_or_distr.
  rewrite eq.
  rewrite or_false.
  now rewrite and_idempotent.
Qed.

Lemma included_and_or_r: forall n (v1 v2 : Bvector n),
    v1 ^| v2 = v2 -> v1 ^& (v1 ^| ^~ v2) = v1.
Proof.
  intros n v1 v2.
  rewrite inclusion_dual_disjoint.
  apply disjoint_and_or_r.
Qed.




Lemma disjoint_xor_or:
  forall n (v1 v2:Bvector n),
    v1 ^& v2 = ⊥_n ->
    v1 ^⊕ (v1 ^| v2) = v2.
Proof.
  intros n v1 v2.
  vin2 v1 v2; simpl; intros eq; trivial.
  destruct (Vector.cons_inj eq) as [eq_hd eq_tl].
  rewrite (rect eq_tl).
  destruct x1 eqn:eq1, x2 eqn:eq2; trivial.
  simpl in eq.
  discriminate.
Qed.

Lemma included_xor_or:
  forall n (v1 v2:Bvector n),
    v1 ^| v2 = v2 ->
    v1 ^⊕ (v1 ^| ^~v2) = ^~v2.
Proof.
  intros n v1 v2.
  rewrite inclusion_dual_disjoint.
  apply disjoint_xor_or.
Qed.

Lemma disjoint_eq_incl_cpt:
  forall n (v1 v2:Bvector n),
    v1 ^& v2 = ⊥_n <-> v1 ^| ^~ v2 = ^~v2.
Proof.
  intros n v1 v2.
  rewrite inclusion_dual_disjoint.
  now rewrite double_neg.
Qed.


