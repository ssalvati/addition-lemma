Require Export Coq.Bool.Bool.
Require Export Coq.Bool.Bvector.
Export Coq.Bool.Bvector.BvectorNotations.
Require Vector.
Export Vector.VectorNotations.
Require PeanoNat.
Require Import Lia.

(* This library contains basic lemmas about boolean combinations for bit-vectors *)
Require Import Bvector_utils.

Set Implicit Arguments.

(** We work with dependently typed Vectors. The type Vector.t A n denotes
vectors:

 - containing data of type A and

 - of length n

We are more specifically interested in bit-vectors of length n, that is values
of types Vector.t bool n. This type is also named BVector n.

The module Coq.Bool.Bvector allows us to use usual bitwise operations :

- ^& : bitwise conjunction

- ^| : bitwise disjunction

- ^⊕ : bitwise xor

- ^~ : bitwise negation

- =? : bitwise equality

The module BVector_utils introduces the notations :

- ⊥_n which denotes the bit-vector of length n with false at each position,

- ⊤_n which denotes the bit-vector of length n with true at each position.

A first goal of this coq file is to show how addition can be represented by
means of:

- bitwise operations,

- the next-until operator (the so-called 'xu operator') of forward-LTL (noted ^#
  in this file).

Conversely (and more importantly) it contains a proof that the xu modality can
be represented by means of:

- bitwise operations,

- the addition (noted ^+ in this file).

This equi-expressivity between addition and the xu modality was known (c.f.
Olivier Serre's 2004 TCS paper 'Vectorial languages and linear temporal logic')
however a precise proof and statement was missing. Formalizing it in coq is a way
of having precise statements and proofs with boring details that one can
trust.

A second and main goal of this coq file is to prove formally Lemma 1 (the
addition lemma) of the paper. This lemma states that (using coq notations):

when we define v to be (y ^+ (y ^| ^~ z) ^& x) (i.e. (y + (y ∨ ¬z)) ∧ x in the
paper),

then, provided that ^~ x ^| z = ⊤ and ^~ y ^| z= ⊤, for all positions i

v[i] = 1 iff x = 1 ∧ ∃ k. k < i ∧ y[k] = 1 ∧ ∀ j. k < j < i ⇒ z[j] = 0.

In the paper, v is denoted by Successor(x,y,z).

For technical reasons we explain below, this coq formalization adopts a view
point that is dual to that of the paper. In the paper the addition traverses
vectors from left to right, while in this formalization traverses it from right
to left. As a result, our statement is that

v[i] = 1 iff x = 1 ∧ ∃ k. i < k ∧ y[k] = 1 ∧ ∀ j. i < j < k ⇒ z[j] = 0.

 *)


(** The function zip_fold is made to combine two vectors of dimension n into one
    vector of dimension n while propagating information from the right of the
    two vectors to their left. The information at a position i depends on:

    - the values of the two vectors at that position, and on

    - the aggregated information computed from the positions (i+1) to n in those
    two vectors.

Suppose we are given two vectors of dimension n a and b that can respectively be
represendted as:

|----+----+-----+----+-----+----|
| a1 | a2 | ... | ai | ... | an |
|----+----+-----+----+-----+----|

and,

|----+----+-----+----+-----+----|
| b1 | b2 | ... | bi | ... | bn |
|----+----+-----+----+-----+----|

then, given a function f and some d, zip_fold f d a b computes the pair
(c,d1) where c is a vector of dimension n represented by:

|----+----+-----+----+-----+----|
| c1 | c2 | ... | ci | ... | cn |
|----+----+-----+----+-----+----|

so that (ci,di) = f ai bi d{i+1} with d{n+1} = d.

Here di represents the information that is propagated form right to left.

Note that in the paper, the information is propagated from left to right while
here it is from right to left. The reason is :

1. In the paper, the left to right propagation is preferred as it is the
preferred reading direction in English.

2. In coq, the right to left propagation is preferred as it simplifies
programming with dependently typed vectors.

The function zip_fold allows us to define information propagating functions on
bit vectors such as forward-LTL modalities or addition (where the carry is
propagated). For addition, as the carry is propagated from right to left, we
make the assumption that the least significant bit is on the right.

 *)

Definition zip_fold A B C D (f: A -> B -> D -> (C*D)%type) (d:D)
           n (v1 : Vector.t A n) (v2 : Vector.t B n) : (Vector.t C n * D)%type :=
  Vector.rect2 (fun n _ _ => (Vector.t C n * D)%type)
               (Vector.nil C, d)
               (fun n v1 v2 res a b =>
                  let (vec, d):= res in
                  let (c, d'):= f a b d in
                  (Vector.cons C c n vec, d')
               ) v1 v2.


(** This lemma shows the unsurprising property that using zip_fold on
extensionally equivalent functions produces extensionally equivalent functions.
*)

Lemma zip_fold_same_function: forall A B C D (f g: A -> B -> D -> (C*D)%type) (d:D)
                                     n (v1 : Vector.t A n) (v2 : Vector.t B n),
    (forall a b d, f a b d = g a b d) ->
    zip_fold f d v1 v2 = zip_fold g d v1 v2.
Proof.
  intros.
  vin2 v1 v2.
  - reflexivity.
  - simpl.
    rewrite rect.
    destruct (zip_fold g d v1' v2').
    now rewrite H.
Qed.



(** Here we define the addition with carry and carry propagation from which we
    can build addition. So add_carry b1 b2 c computes a pair (b,c') where b is the
bit obtained by summing b1 b2 and c, c' is then the carry that we propagate
for the continuation of the addition. *)


Definition add_carry (b1 b2 carry:bool): bool * bool:=
  (xorb b1 (xorb b2 carry),
    b1 && (b2 || carry) ||
      carry && (b1 || b2)).

(** The function carry_local is similar to add_carry, but instead of
computing the addition, it just produces a vector where the positions contain
true if and only if a carry is used at this position when computing the
addition. *)

Definition carry_local (b1 b2 carry:bool): bool*bool:=
  (carry, b1 && (b2 || carry) ||
            carry && (b1 || b2)).

(** Here follows an exhaustive sequence of tests for the function add_carry. *)

Example cTTT: add_carry true true true = (true, true).
Proof. reflexivity. Qed.

Example cTTF: add_carry true true false = (false, true).
Proof. reflexivity. Qed.

Example cTFT: add_carry true false true = (false, true).
Proof. reflexivity. Qed.

Example cTFF: add_carry true false false = (true, false).
Proof. reflexivity. Qed.

Example cFTT: add_carry false true true = (false, true).
Proof. reflexivity. Qed.

Example cFTF: add_carry false true false = (true, false).
Proof. reflexivity. Qed.

Example cFFT: add_carry false false true = (true, false).
Proof. reflexivity. Qed.

Example cFFF: add_carry false false false = (false, false).
Proof. reflexivity. Qed.

(** Applying zip_fold to add_carry and carry_local allows us to obtain addition
    (BVadd) and the fonction BVcarry that produces vectors which contain the
positions where a carry is used in the addition. Notice that these functions take
a boolean argument that corresponds to the value of the initial carry they start
with. In general, this initial carry should be set to false meaning that there
is no carry when we start the computation. It is however also possible to start
with true as a carry. *)

Definition BVadd := zip_fold add_carry.

Definition BVcarry := zip_fold carry_local.

(** We define a particular circuit based on carry_local that will prove useful. *)

Definition carry_local_orb a b c :=  carry_local a (a || b) c.

Definition BVcarry_orb := zip_fold carry_local_orb.

(** We use the infix notation ^+ to denote the sum of two vectors. *)

Notation "v1 ^+ v2" := (let (v,_):=BVadd false v1 v2 in v) (at level 40, left associativity) : Bvector_scope.

(* Definition until_local := fun a b c => (b || a && c, b || a && c). *)

(** We now define locally the binary connective "next until" that we write XU or
^#. Intuitively v1 XU v2 produces a vector where a position i is set to true
when there is a position k so that v1[k]=true and for all j so that i<j<k,
v2[j]=true.

 Recall here that zip_fold computes from right to left in vectors. So mainly we
 use a sort of "carry" bit that contains the information whether:
 - the bit following v1 is set to true, or
 - the bit following v2 is set to true and we had a carry.
 *)

Definition xu_local := fun a b c => (c, a || b && c).

(** We compose the definition of xu_local with conjunction and disjunction. This
operation is useful for us when we are showing how to translate xu, BVcarry and
BVadd into each other. *)

Definition xu_local_andb_orb := fun a b c => xu_local (a && b) (a || b) c.

(* Definition until := zip_fold until_local. *)

Definition xu := zip_fold xu_local.

(** As we explained, we use ^# as the infix operation denoting XU. *)
Notation "v1 ^# v2" := (let (v,_):= xu false v2 v1 in v) (at level 40, left associativity) : Bvector_scope.

Definition xu_andb_orb := zip_fold xu_local_andb_orb.


(** We test our definition of xu. So as to be convinced it works, one needs to
read the two input vectors from right to left. *)
Example xu_ex: (xu false
                   [false; false; false; true ; false; false; true ; false; true ]
                   [true ; false; true ; false; false; true ; true ; false; true ])=
                 ( [false; true ; true ; false; true ; true ; false; true ; false], false).
Proof.
  reflexivity.
Qed.

(** We now prove a bunch of simple identities mixing bitwise operations. *)
Lemma xu_local_orb_r: forall a b c,
    xu_local a b c = xu_local a (a || b) c.
Proof.
  intros a b c.
  now destruct a,b,c.
Qed.

Lemma xu_local_and_carry_local_orb: forall a b c,
    xu_local a b c = carry_local_orb a b c.
Proof.
  intros a b c.
  now destruct a, b, c.
Qed.

Lemma xu_local_andb_orb_and_carry_local: forall a b c,
    xu_local_andb_orb a b c = carry_local a b c.
Proof.
  intros a b c.
  now destruct a,b,c.
Qed.

Lemma carry_local_and_add_carry: forall a b c,
    carry_local a b c = (xorb (fst (add_carry a b c)) (xorb a b), snd (add_carry a b c)).
Proof.
  intros a b c.
  now destruct a,b,c.
Qed.

(** We now show that we can express BVcarry by means of BVadd and bitwise XOR (noted ^⊕). *)

Lemma BVcarry_from_BVadd: forall n (v1 v2 add: Bvector n) r c,
    BVadd c v1 v2 = (add, r) ->
    BVcarry c v1 v2 = (add ^⊕ v1 ^⊕ v2, r).
Proof.
  intros n v1 v2.
  vin2 v1 v2; simpl; intros add r c eq.
  - now inversion eq.
  - destruct (BVadd c v1' v2') eqn:eq_add.
    rewrite (rect _ _ _ eq_add).
    inversion eq.
    simpl.
    replace (xorb (xorb (xorb x1 (xorb x2 b)) x1) x2) with b; trivial.
    destruct x1, x2, b; reflexivity.
Qed.

(** We show the obvious identity between BVcarry_orb and BVcarry. *)

Lemma BVcarry_orb_and_carry_local:
  forall n (v1 v2: Bvector n) c,
    BVcarry_orb c v1 v2 = BVcarry c v1 (v1 ^| v2).
Proof.
  intros n v1 v2 c.
  vin2 v1 v2; trivial.
  simpl in *.
  destruct (BVcarry_orb c v1' v2') eqn:eq1.
  destruct (BVcarry c v1' (v1' ^| v2')) eqn:eq2.
  now inversion rect.
Qed.

(** We finally show that xu and BVcarry_orb are in fact the same function. *)

Lemma xu_and_BVcarry_orb:
  forall n (v1 v2:Bvector n) c,
    xu c v1 v2 = BVcarry_orb c v1 v2.
Proof.
  intros.
  apply zip_fold_same_function.
  apply xu_local_and_carry_local_orb.
Qed.

(** Similarly to BVcarry_orb, we have the obvious identity between xu and
xu_andb_orb. *)

Lemma xu_and_xu_andb_orb:
  forall n (v1 v2:Bvector n) c,
    xu c (v1 ^& v2) (v1 ^| v2) = xu_andb_orb c v1 v2.
Proof.
  intros n v1 v2 c.
  vin2 v1 v2; intros; trivial.
  simpl.
  now rewrite rect.
Qed.

(** Combining these identities, we can express BVcarry by means of xu and
boolean operations. *)

Theorem xu_and_BVcarry:
  forall n (v1 v2 :Bvector n) c,
    xu c (v1 ^& v2) (v1 ^| v2) = BVcarry c v1 v2.
Proof.
  intros.
  rewrite xu_and_xu_andb_orb.
  apply zip_fold_same_function.
  apply xu_local_andb_orb_and_carry_local.
Qed.

(** This finally allows us to express xu with BVadd. *)

Theorem xu_and_BVadd:
  forall n (v1 v2 add :Bvector n) r c,
    BVadd c v1 (v1 ^| v2) = (add, r)->
    xu c v1 v2 = (add ^⊕ v1 ^⊕ (v1 ^| v2), r).
Proof.
  intros n v1 v2 add r c eq.
  rewrite xu_and_BVcarry_orb.
  rewrite BVcarry_orb_and_carry_local.
  now  rewrite (BVcarry_from_BVadd _ _ _ eq).
Qed.

(** When we forget about the last carry computed, we can express the previous
identity only with operations combining bit-vectors. *)

Theorem next_until_and_addition:
  forall n (v1 v2 :Bvector n),
    (v1 ^# v2) = (v2 ^+ (v2 ^| v1)) ^⊕ v2 ^⊕ (v2 ^| v1).
Proof.
  intros n v1 v2.
  destruct (xu false v2 v1) eqn:eq_xu.
  destruct (BVadd false v2 (v2 ^| v1)) eqn:eq_add.
  rewrite (xu_and_BVadd v2 v1 false eq_add) in eq_xu.
  now inversion eq_xu.
Qed.


(** We now turn to the converse translation and show how to express BVadd by
means of xu and boolean operations. *)

Theorem BVadd_and_xu:
  forall n (v1 v2 xu_res:Bvector n) r c,
    xu c (v1 ^& v2) (v1 ^| v2) = (xu_res, r) ->
    BVadd c v1 v2 = (v1 ^⊕ v2 ^⊕  xu_res,r).
Proof.
  intros n v1 v2 xu_res r c eq.
  rewrite xu_and_BVcarry in eq.
  generalize dependent xu_res.
  generalize dependent r.
  generalize dependent c.
  vin2 v1 v2; simpl; intros c r xu_res eq.
  - now inversion eq.
  - destruct (BVcarry c v1' v2') eqn:eq_bvcarry; simpl in *.
    inversion eq.
    rewrite (rect _ _ _ eq_bvcarry).
    simpl.
    now rewrite xorb_assoc.
Qed.

(** Forgetting about the last carry computed, we obtain the following identity. *)

Theorem addition_and_next_until:
  forall n (v1 v2 :Bvector n),
    v1 ^+ v2 = v1 ^⊕ v2 ^⊕ ((v1 ^| v2) ^# (v1 ^& v2)).
Proof.
  intros n v1 v2.
  destruct (xu false  (v1 ^& v2) (v1 ^| v2)) eqn:eq_xu.
  destruct (BVadd false v1 v2) eqn:eq_add.
  rewrite (BVadd_and_xu _ _ _ eq_xu) in eq_add.
  now inversion eq_add.
Qed.

(** We now turn to the proof of the addition Lemma. We have two versions of this
lemma: one that is based on bit-vectors disjointness and the other based on
inclusion (as in the paper).

Two vectors v1 and v2 are considered disjoint when: v1 ^& v2 = ⊥_n.

The vector v1 is considered included in v2 when: v1 ^| v2 = v2.

As v1 ^| v2 = v2 is equivalent to v1 ^& ^~ v2 = ⊥_n, we prove a version of the
addition lemma based on disjointness and then prove it as in the paper.

We start with particular identities using addition and next-until.
 *)

Lemma disjoint_addition_and_xu:
  forall n (v1 v2:Bvector n),
           v1 ^& v2 = ⊥_n ->
           v1 ^+ (v1 ^| v2) = v2 ^⊕ (v1 ^| v2) ^# v1.
Proof.
  intros n v1 v2 disjoint.
  rewrite addition_and_next_until.
  rewrite (disjoint_and_or_r _ _ disjoint).
  rewrite or_absorb.
  now rewrite (disjoint_xor_or _ _ disjoint).
Qed.

Lemma disjoint_xu_and_addition:
  forall n (v1 v2:Bvector n),
    v1 ^& v2 = FALSE n ->
    (v2 ^# v1) = (v1 ^+ (v1 ^| v2)) ^⊕ v2.
Proof.
  intros n v1 v2 disjoint.
  rewrite next_until_and_addition.
  rewrite xor_assoc.
  rewrite xor_disj.
  now rewrite (not_and_disjoint _ _ disjoint).
Qed.


Lemma disjoint_xu_xor_eq_addition:
  forall n (v1 v2:Bvector n),
    v1 ^& v2 = FALSE n ->
    (v2 ^# v1) ^⊕ v2 = (v1 ^+ (v1 ^| v2)).
Proof.
  intros n v1 v2 eq.
  rewrite (disjoint_xu_and_addition _ _ eq).
  rewrite xor_assoc.
  rewrite xor_nilpotent.
  now rewrite xor_false.
Qed.

Lemma disjoint_xu_nand_eq_addition_nand:
  forall n (v1 v2:Bvector n),
    v1 ^& v2 = FALSE n ->
    (v2 ^# v1) ^& ^~ v2 = (v1 ^+ (v1 ^| v2)) ^& ^~v2.
Proof.
  intros n v1 v2 eq.
  rewrite <- (disjoint_xu_xor_eq_addition _ _ eq).
  now rewrite xor_conj.
Qed.


Lemma xu_and_addition_with_disjointness_hypotheses:
  forall n (v1 v2 v3:Bvector n),
    v2  ^& v3 = FALSE n ->
    v3  ^& v1 = FALSE n ->
    (v3 ^# v2) ^& v1 = (v2 ^+ (v2 ^| v3)) ^& v1.
Proof.
  intros n v1 v2 v3 disjoint23 disjoint31.
  rewrite (disjoint_xu_and_addition _ _ disjoint23).
  now rewrite (xor_and_distr_disjoint _ _ _ disjoint31).
Qed.


(** We now relate the next-until operation with its description in first-order
logic.

We have not formalized first-order logic on bit-vectors in coq. Rather, we
express within coq logic such properties. For this we need to use positions in
vectors. As vectors are dependently typed, these positions are represented by
objects of type {k | k<n} (where n is the length of vectors). Such objects are
pairs, one being an number k and the other being a proof that k<n.

So as to use positions in an intuitive manner we introduce notations :

- v{@ p} is the value of the vector v at position p

- ⟨p⟩ is the number that is contained in position p (which is an object of type
  {k | k<n}).

 *)

Notation "v {@ sig }" := v[@ Fin.of_nat_lt (proj2_sig sig)] (at level 1, format "v {@ sig }") : vector_scope.

Notation "⟨ sig ⟩" := (proj1_sig sig) (at level 1, format "⟨ sig ⟩") : vector_scope.


(** Specification of xu with first-order logic.

We start by describing when the carry used by next-until is true at the end of
the computation v1 ^# v2 on bit-vectors of length n. The coq formalization
expresses this is the case iff

∃ k, k<n ∧ v1[k]=1 ∧ ∀ j, j < k ⇒ v2[j]=1

 *)
Lemma xu_in_fo_partial:
  forall n (v1 v2 xu_res:Bvector n) r,
    xu false v1 v2 = (xu_res, r) ->
    r=true <->
      exists (sk : {k | k <n}),
        v1{@ sk} =true /\
          forall (sj : {k | k<n}) (j_lt_k: ⟨sj⟩ < ⟨sk⟩),
            v2{@ sj}=true.
Proof.
  intros n v1 v2.
  vin2 v1 v2; intros xu_res r eq.
  - inversion eq.
    split.
    + discriminate.
    + intros [[k k_lt_n]].
      inversion k_lt_n.
  - split.
    + destruct r; try discriminate.
      intro stupid; clear stupid.
      simpl in eq.
      destruct (xu false v1' v2') as (xu_res', r') eqn:eq_xu.
      specialize (@rect  xu_res' r' eq_refl ).
      destruct x1, x2; simpl in eq.
      ++ assert (0 < S n0) as ineq by lia.
         exists (exist _ 0 ineq).
         split; trivial.
         simpl; lia.
      ++  assert (0 < S n0) as ineq by lia.
          exists (exist _ 0 ineq).
          split; trivial.
          simpl; lia.
      ++ destruct r'.
      * destruct rect as [ex stupid]; clear stupid.
        specialize (ex eq_refl).
        destruct ex as [[k k_lt_n] [k_true rest_true]].
        assert (S k < S n0) as ineq by lia.
        simpl in k_true.
        exists  (exist _ (S k) ineq).
        simpl.
        split.
        ** now rewrite <- (Fin.of_nat_ext k_lt_n).
        **  intros.
            destruct sj as [[|j] j_lt_n] eqn:eq_sj.
        *** trivial.
        *** simpl.
            assert (j < n0) as ineqj by lia.
            specialize (rest_true (exist _ j ineqj)).
            simpl in *.
            rewrite <- (Fin.of_nat_ext ineqj).
            apply rest_true.
            lia.
      * discriminate.
        ++ discriminate.
    + simpl in eq.
      destruct (xu false v1' v2') as [xu_res' r']eqn:eq_xu.
      intros ex.
      specialize (rect xu_res' r' eq_refl).
      destruct ex as [[[|k] k_lt_sn0] [eq_k rest]]; simpl in eq_k.
      ++ now rewrite eq_k in eq; inversion eq.
      ++ assert (r'=true) as eq_r'.
      * rewrite rect.
        assert (k<n0) as ineq by lia.
        exists (exist _ k ineq).
        split.
        ** now rewrite <- (Fin.of_nat_ext (Lt.lt_S_n k n0 k_lt_sn0)).
        ** intros [j j_lt_n] j_lt_k.
           simpl in *.
           assert (S j < S k) as sj_lt_sk by lia.
           assert (S j < S n0) as sj_lt_sn by lia.
           specialize (rest (exist _ (S j) sj_lt_sn)).
           simpl in rest.
           specialize (rest sj_lt_sk).
           simpl in rest.
           now rewrite <- (Fin.of_nat_ext (Lt.lt_S_n j n0 sj_lt_sn)).
      * assert (0 < S n0) as ineq by lia.
           specialize (rest (exist _ 0 ineq)).
           simpl in rest.
           rewrite rest, eq_r' in eq; [| lia].
           simpl in eq.
           now rewrite orb_true_r in eq; inversion eq.
Qed.

(** The previous lemma allows us to express the logical properties of the result
of v1 ^# v2. *)

Lemma xu_in_fo_raw:
  forall n (v1 v2 xu_res:Bvector n) r,
    xu false v1 v2 = (xu_res, r) ->
    forall si : {k | k < n},
      xu_res{@ si} = true <->
      exists sk : {k | k < n},
        ⟨si⟩ < ⟨sk⟩ /\
        v1{@ sk} =true /\
          forall (sj : {k | k <n})
                 (i_lt_j:  ⟨si⟩ < ⟨sj⟩)
                 (j_lt_k: ⟨sj⟩ < ⟨sk⟩),
            v2{@ sj}=true.
Proof.
  intros n v1 v2.
  vin2 v1 v2; intros xu_res r eq_xu [i i_lt_n].
  - lia.
  - simpl in eq_xu.
    destruct (xu false v1' v2') as [xu_res' r'] eqn:eq_xu'.
    rewrite (inv_vecS xu_res) in *.
    inversion eq_xu.
    destruct i.
    + split.
      * intros eq_true.
        simpl in eq_true.
        rewrite eq_true in H0.
        rewrite (xu_in_fo_partial _  _ eq_xu') in H0.
        destruct H0 as [[k k_lt_k] [P1 P2]].
        assert (S k < S n0) as ineq by lia.
        exists (exist _ (S k) ineq).
        simpl.
        split; [lia| split].
        ** simpl.
           now rewrite <- (Fin.of_nat_ext (k_lt_k)).
        ** intros [j j_lt_sn] O_lt_j j_lt_sk; simpl in *.
           destruct j.
           *** inversion O_lt_j.
           *** simpl.
               assert (j < k ) as j_lt_k by lia.
               assert (j < n0 ) as j_lt_n by lia.
               specialize (P2 (exist _ j j_lt_n)).
               simpl in P2.
               rewrite <- (Fin.of_nat_ext (j_lt_n)).
               now apply P2.
      * intros [[k k_lt_n] [i_lt_k [P1 P2]]].
        simpl in i_lt_k.
        rewrite <- H0.
        rewrite (xu_in_fo_partial _  _ eq_xu').
        destruct k  as [|k]; [lia|].
        simpl in P1.
        assert (k < n0) as k_lt_n' by lia.
        exists (exist _ k k_lt_n').
        split.
        ** now rewrite <- (Fin.of_nat_ext (Lt.lt_S_n k n0 k_lt_n)).
        ** intros [j j_lt_n] j_lt_k.
           simpl in j_lt_k.
           assert (0 < S j) as O_lt_sj by lia.
           assert (S j < S k) as sj_lt_sk by lia.
           assert (S j < S n0) as sj_lt_sn by lia.
           specialize (P2 (exist _ (S j) sj_lt_sn)).
           simpl in P2.
           rewrite <- (Fin.of_nat_ext (Lt.lt_S_n j n0 sj_lt_sn)).
           now apply P2.
    + simpl ((Vector.hd xu_res :: Vector.tl xu_res){@ exist (fun k : nat => k < S n0) (S i) i_lt_n} = true) .
      simpl in eq_xu.
      specialize (rect xu_res' r' eq_refl  (exist _ i (Lt.lt_S_n i n0 i_lt_n))).
      rewrite (pair_equal_spec) in eq_xu.
      destruct eq_xu as [eq_xu_res eq_r].
      destruct  (Vector.cons_inj eq_xu_res) as [eq_hd eq_tl].
      rewrite <- eq_tl.
      rewrite rect.
      split; intros [[k k_lt_n] [i_lt_k [eq_true P] ]].
      * assert ( S k < S n0) as sk_lt_sn by lia.
        exists (exist _ (S k) sk_lt_sn).
        simpl in *.
        split; try lia.
        split.
        ** simpl.
           now rewrite <- (Fin.of_nat_ext k_lt_n).
        ** intros [j j_lt_sn] si_lt_j j_lt_sk.
           destruct j; simpl in *; try lia.
           apply Lt.lt_S_n in si_lt_j.
           apply Lt.lt_S_n in j_lt_sk.
           assert (j < n0) as j_lt_n by lia.
           specialize  (P (exist _ j j_lt_n)).
           simpl in P.
           rewrite <- (Fin.of_nat_ext j_lt_n); tauto.
      * destruct k; simpl in *; try lia.
        simpl in eq_true.
        exists (exist _ k (Lt.lt_S_n k n0 k_lt_n)).
        simpl.
        split; try lia.
        split; trivial.
        intros [j j_lt_n] i_lt_j j_lt_k.
        simpl in *.
        assert (S i < S j) as si_lt_sj by lia.
        assert (S j < S k) as sj_lt_sk by lia.
        assert (S j < S n0) as sj_lt_sn by lia.
        specialize (P (exist _ (S j) sj_lt_sn)).
        simpl in P.
        rewrite <- (Fin.of_nat_ext (Lt.lt_S_n j n0 sj_lt_sn)); tauto.
Qed.

(** Now wrapping the previous lemma, we can express the meaning of next-until
    given two bit-vectors v1 and v2 of length n:

 ∀ i<n, (v1 ^# v2)[i]=1 ⇔ (∃ k<n, i < k ∧ v1[i]=1 ∧ ∀ j<n, i < j < k ⇒ v2[j]=1)

 *)

Lemma xu_in_fo:
  forall n (v1 v2:Bvector n),
    forall si : {k | k < n},
      (v2 ^# v1){@ si} = true <->
      exists sk : {k | k <n},
        ⟨si⟩ < ⟨sk⟩ /\
        v1{@ sk} =true /\
          forall (sj : {k | k<n})
                 (i_lt_j: ⟨si⟩ < ⟨sj⟩)
                 (j_lt_k: ⟨sj⟩ < ⟨sk⟩),
            v2{@ sj}=true.
Proof.
  intros n v1 v2.
  destruct (xu false v1 v2) eqn:eq_xu.
  apply (xu_in_fo_raw _ _ eq_xu).
Qed.

(** FO specification of the "disjoint"-based addition lemma.

It states that given three bit-vectors v1, v2, v3 of length n:

when v1 ∧ v3 = 0 and v3 ∧ v2 = 0, we have forall i<n:

((v2 + (v2 ∨ v3)) ∧ v1)[i] = 1

iff

v1[i] = 1 ∧ ∃ k<n. i < k ∧ v[k] = 1 ∧ ∀ j<n. i < j < k ⇒ v3[j] = 1.

With the left to right convention of the paper it would state that forall i<n:

((v2 + (v2 ∨ v3)) ∧ v1)[i] = 1

iff

v1[i] = 1 ∧ ∃ k<n. k < i ∧ v[k] = 1 ∧ ∀ j<n. k < j < i ⇒ v3[j] = 1.

 *)

Lemma disjoint_addition_lemma:
  forall n (v1 v2 v3:Bvector n),
     v2  ^& v3 = FALSE n ->
    v3  ^& v1 = FALSE n ->
    forall si : {k| k<n},
      ((v2 ^+ (v2 ^| v3)) ^& v1){@ si} = true <->
        (v1{@ si} = true /\
           exists sk : {k|k<n},
               ⟨si⟩ < ⟨sk⟩ /\
                 v2{@ sk} =true /\
                 forall (sj : {k | k <n})
                        (i_lt_j: ⟨si⟩ < ⟨sj⟩)
                        (j_lt_k: ⟨sj⟩ < ⟨sk⟩),
                    v3{@ sj}=true).
Proof.
  intros n v1 v2 v3 disjoint23 disjoint31 [i i_lt_n].
  rewrite <- xu_and_addition_with_disjointness_hypotheses; trivial.
  rewrite and_andb.
  rewrite andb_true_iff.
  rewrite xu_in_fo.
  tauto.
Qed.

Lemma neg_true:
    forall n (v:Bvector n)(si: {k | k <n}),
      (^~ v){@ si} = true <-> v{@ si} = false.
Proof.
  intros n v.
  induction v; intros si.
  - destruct si.
    inversion l.
  - destruct si.
    destruct x.
    + simpl.
      now destruct h.
    + simpl.
      apply (IHv (exist (fun x => x <n) x (Lt.lt_S_n x n l))).
Qed.

(** We finally prove the addtion lemma.

It states that given three bit-vectors v1, v2, v3 of length n:

when v1 ^| v3 = v3 and v2 ^| v3 = v3 (i.e. both v1 and v2 are included in v3),
we have forall i<n:

((v2 + (v2 ∨ ¬v3)) ∧ v1)[i] = 1

iff

v1[i] = 1 ∧ ∃ k<n. i < k ∧ v[k] = 1 ∧ ∀ j<n. i < j < k ⇒ v3[j] = 0.

With the left to right convention of the paper it would state that forall i<n:

((v2 + (v2 ∨ ¬v3)) ∧ v1)[i] = 1

iff

v1[i] = 1 ∧ ∃ k<n. k < i ∧ v[k] = 1 ∧ ∀ j<n. k < j < i ⇒ v3[j] = 0.

In the paper the vector ((v2 + (v2 ∨ ¬v3)) ∧ v1) is called Successor(v1,v2,v3).
 *)

Lemma addition_lemma:
  forall n (v1 v2 v3:Bvector n),
     v2  ^| v3 = v3 ->
    v1  ^| v3 =  v3 ->
    forall si : {k| k<n},
      ((v2 ^+ (v2 ^| ^~ v3)) ^& v1){@ si} = true <->
        (v1{@ si} = true /\
           exists sk : {k|k<n},
               ⟨si⟩ < ⟨sk⟩ /\
                 v2{@ sk} =true /\
                 forall (sj : {k | k <n})
                        (i_lt_j: ⟨si⟩ < ⟨sj⟩)
                        (j_lt_k: ⟨sj⟩ < ⟨sk⟩),
                    v3{@ sj}=false).
Proof.
  intros n v1 v2 v3.
  repeat rewrite inclusion_dual_disjoint.
  rewrite (and_comm v1 (^~ v3)).
  intros  disjoint23 disjoint31 si.
  rewrite (disjoint_addition_lemma v1 v2  (^~v3) disjoint23 disjoint31 si).
  now setoid_rewrite neg_true.
Qed.


